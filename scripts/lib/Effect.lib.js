/**
 * All Effects are functions that take 2 Pokemon arguments.
 * Effects can be added to {@see Skill}s to do things like giving {@see Status}.
 * Effects happen once per skill use. Statuses have a duration and activate before all attacks.
 */
import Pokemon from '../class/pokemon/Pokemon.js';
import { AtkUp, Burn, Flinch, Freeze, MoveFast, MoveSlow, Paralyze } from './Status.lib.js';

/**
 * @param {Pokemon} self 
 */
export function atkBuff(self, targets) { self.statusCondition.push(AtkUp()); }

/**
 * @param {Pokemon[]} targets 
 */
export function burnDebuff(self, targets) { targets.forEach(target => target.statusCondition.push(Burn())); }

/**
 * @param {Pokemon[]} targets 
 */
export function flinchDebuff(self, targets) { targets.forEach(target => target.statusCondition.push(Flinch())); }

/**
 * @param {Pokemon[]} targets 
 */
export function freezeDebuff(self, targets) { targets.forEach(target => target.statusCondition.push(Freeze())); }

/**
 * @param {Pokemon} self 
 */
export function moveFastBuff(self, targets) { self.statusCondition.push(MoveFast()); }

/**
 * @param {Pokemon[]} targets 
 */
export function moveSlowDebuff(self, targets) { targets.forEach(target => target.statusCondition.push(MoveSlow())); }

/**
 * @param {Pokemon[]} targets 
 */
export function paralyzeDebuff(self, targets) { targets.forEach(target => target.statusCondition.push(Paralyze())); }
