import { AoeShape } from '../class/enums/AoeShape.js';
import { Category } from '../class/enums/Category.js';
import Skill from '../class/pokemon/Skill.js';
import { TargetMode } from '../class/enums/TargetMode.js';
import { Type } from '../class/enums/Type.js';
import { atkBuff, burnDebuff, flinchDebuff, freezeDebuff, moveFastBuff, moveSlowDebuff, paralyzeDebuff } from './Effect.lib.js'

//----------------------------- Skill Name ---------- Category,      Type,     Power,  PP, Range,    AoeShape, TargetMode,     Effect
export const BasicMelee   = new Skill('Basic Melee',  Category.PHYS, Type.NULL,   10,   0, 1, AoeShape.SINGLE, TargetMode.FOE);
export const BasicRange   = new Skill('Basic Range',  Category.SPEC, Type.NULL,    6,   0, 2, AoeShape.SINGLE, TargetMode.FOE);
export const Agility      = new Skill('Agility',      Category.SPEC, Type.PSYCH,   0,   5, 1, AoeShape.SINGLE, TargetMode.ALLY_OR_SELF).addEffect(moveFastBuff);
export const Bubble       = new Skill('Bubble',       Category.SPEC, Type.WATER,  20,   3, 2, AoeShape.SINGLE, TargetMode.FOE).addEffect(moveSlowDebuff);
export const BubbleBeam   = new Skill('Bubble Beam',  Category.SPEC, Type.WATER,  65,   5, 2, AoeShape.LONG2,  TargetMode.FOE).addEffect(moveSlowDebuff);
export const DarkPulse    = new Skill('Dark Pulse',   Category.SPEC, Type.DARK,   80,   8, 1, AoeShape.CIRCLE, TargetMode.SELF).addEffect(flinchDebuff);
export const Flamethrower = new Skill('Flamethrower', Category.SPEC, Type.FIRE,   90,  10, 4, AoeShape.LONG4,  TargetMode.FOE).addEffect(burnDebuff);
export const IceBeam      = new Skill('Ice Beam',     Category.SPEC, Type.ICE,    90,  10, 4, AoeShape.LONG4,  TargetMode.FOE).addEffect(freezeDebuff);
export const SwordsDance  = new Skill('Swords Dance', Category.STAT, Type.NORML,   0,   5, 1, AoeShape.SINGLE, TargetMode.ALLY_OR_SELF).addEffect(atkBuff);
export const Thunder      = new Skill('Thunder',      Category.SPEC, Type.ELEC,  120,  12, 3, AoeShape.ALL7,   TargetMode.FOE).addEffect(paralyzeDebuff);
export const Earthquake   = new Skill('Earthquake',   Category.PHYS, Type.GRND,  100,  15, 2, AoeShape.ALL19,  TargetMode.SELF);
