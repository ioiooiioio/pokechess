import Pokemon from '../class/pokemon/Pokemon.js'
import Status from '../class/pokemon/Status.js'

const BUFF   = true;
const DEBUFF = false;

/**
 * @param {Pokemon} target
 */
const atkUp = (target) => target.current.ATK *= 2;
export const AtkUp = () => new Status(atkUp, BUFF, 5);

/**
 * @param {Pokemon} target
 */
const burn = (target) => {
  const isFrozen = target.statusCondition.find(status => status.effect === freeze);
  if (isFrozen) { isFrozen.duration = 0; }
  target.current.ATK /= 2;
  target.current.HP -= Math.floor(target.base.HP / 8);
}
export const Burn = () => new Status(burn, DEBUFF, 8);

/**
 * @param {Pokemon} target
 */
const flinch = (target) => target.current.SPE = 0;
export const Flinch = () => new Status(flinch, DEBUFF, 1);

/**
 * @param {Pokemon} target
 */
const freeze = (target) => {
  const isBurned = target.statusCondition.find(status => status.effect === burn);
  if (isBurned) { isBurned.duration = 0; }
  target.current.SPE /= 2;
}
export const Freeze = () => new Status(freeze, DEBUFF, 8);

/**
 * @param {Pokemon} target
 */
const moveSlow = (target) => target.current.moveSpeMod /= 2;
export const MoveSlow = () => new Status(moveSlow, DEBUFF, 5);

/**
 * @param {Pokemon} target
 */
const moveFast = (target) => target.current.moveSpeMod *= 2;
export const MoveFast = () => new Status(moveFast, BUFF, 5);

/**
 * @param {Pokemon} target
 */
const paralyze = (target) => target.current.ppGrowthRate = 0;
export const Paralyze = () => new Status(paralyze, DEBUFF, 8);
