import { AllItems } from '../../lib/Item.lib.js';
import { AllPokemon } from '../../lib/Pokemon.lib.js';
import Item from '../items/Item.js';
import Pokemon from '../pokemon/Pokemon.js';

export default class Deck {
  /**
   * @class
   */
  constructor() {
    /** @type {Pokemon[]} */
    const copyOfAllPokemon = AllPokemon.slice(0).map(pkmn => Object.assign(new Pokemon(), pkmn));
    /** @type {Item[]} */
    this.allItems = this.shuffle(AllItems.slice(0));
    /** @type {Pokemon[]} */
    this.allPokemon = this.shuffle(copyOfAllPokemon.slice(0));
  }

  /**
   * @template T
   * @param {Array.<T>} array Shuffles the library using Durstenfeld shuffle, but can be used to shuffle any array
   * @returns {Array.<T>} the shuffled array
   */
  shuffle(array = this.allItems) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

}
