import Pokemon from './Pokemon.js';

/**
 * Modifies the current state of a Pokemon. Can include stat changes, Poison, Sleep, etc.
 */
export default class Status {

  /**
   * @class
   * @param {function} effect A function(target:Pokemon) => void that modifies a Pokemon's stats or other traits
   * @param {boolean} isBuff true if positive status, false if negative status
   * @param {number} duration The amount of turns the Effect lasts. Gets removed at 0. If negative, it lasts forever.
   */
  constructor(effect, isBuff, duration = -1) {
    this.effect   = effect;
    this.isBuff   = isBuff;
    this.duration = duration;
  }

  /**
   * @param {Pokemon} target The Pokemon to apply an effect on
   * @returns {void}
   */
  activate(target) {
    if (this.duration !== 0) {
      this.effect(target);
      this.duration--;
    }
  }

}
