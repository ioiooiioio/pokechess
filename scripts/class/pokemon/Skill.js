import { AoeShape } from '../enums/AoeShape.js';
import { Category } from '../enums/Category.js';
import Pokemon from './Pokemon.js';
import { TargetMode } from '../enums/TargetMode.js';
import { Type } from '../enums/Type.js';

/**
 * Defines a Pokemon's Attacks aka Moves. This class is named 'Skill' to prevent confusion when referring to
 * the 'atk' stat or 'movement' on the board.
 */
export default class Skill {

  /**
   * @class
   * @param {string} name The skill name
   * @param {Category} category Physical uses atk and def. Special uses satk and sdef.
   * @param {Type} type Used to determine the types of Pokemon that can use this move and for type effectiveness
   * @param {number} power Used to multiply the amount of damage dealt in an attack
   * @param {number} ppCost The amount of PP required to consume to use
   * @param {number} range The radius this skill can reach. 0 means it can only target itself, 1 can hit adjacent tiles
   * @param {AoeShape} aoeShape The additional targets this skill could reach in relationship to the target
   * @param {TargetMode} targetMode The targets this Skill is allowed to hit
   */
  constructor(name, category, type, power, ppCost, range, aoeShape, targetMode) {
    this.name       = name;
    this.desc       = '';
    this.category   = category;
    this.type       = type;
    this.power      = power;
    this.ppCost     = ppCost;
    this.range      = range;
    this.aoeShape   = aoeShape;
    this.targetMode = targetMode;
    this.targets    = [];
    /** @type {function[]} */
    this.extraEffects = [];
  }

  /**
   * Queries PokeAPI for a Pokemon Move and uses those values for this Skill. Not recommended for final product,
   * but can be useful for testing purposes when you don't feel like inventing a bunch of Skills.
   * @param {string} name the name of the move
   * @returns {Skill} this object with name, desc, type, power, ppCost defined. extraEffects must be added manually
   */
  async init(name) {
    /**
     * Queries PokeAPI for a Pokemon and defines their stats and types
     * @link https://pokeapi.co/
     */
    const pokeAPI = await fetch('https://pokeapi.co/api/v2/move/' + name.toLowerCase().replace(' ', '-'))
      .then(response => {
        if (response.ok) {
          return response.json()
        } else {
          throw new Error(`[${response.status}] ${response.statusText}\n${response.body}`);
        }
      })
      .catch(err => console.log('Likely a network issue from a rejected Promise\n' + err));

    this.name = name;
    this.desc = pokeAPI.effect_entries.effect;
    this.category = pokeAPI.damage_class.name;
    this.type = pokeAPI.type.name;
    this.power = pokeAPI.power || 0;
    this.ppCost = Math.floor((40 - pokeAPI.pp) / 2);
    return this;
  }

  /**
   * Calculates the amount of damage this Skill should deal based on the attacking and defending Pokemon's stats
   * Current formula is derived roughly from the main Pokemon games
   * @TODO Needs modifiers like super effectiveness and current buffs/debuffs
   * @param {Pokemon} attackingPokemon The attacking Pokemon
   * @param {Pokemon} defendingPokemon The Pokemon getting attacked
   * @returns {number} the amount of damage that would be dealt if attackingPokemon fights defendingPokemon
   */
  damage(attackingPokemon, defendingPokemon) {
    if (this.category === Category.PHYS) {
      var damage = attackingPokemon.current.ATK * this.power / (1.6 * defendingPokemon.current.DEF) + 2;
      console.log(`PHYSICAL DAMAGE ${damage}`);
      return damage;
    } else if (this.category === Category.SPEC) {
      var spDamage = attackingPokemon.current.SATK * this.power / (1.6 * defendingPokemon.current.SDEF) + 2;
      console.log(`SPECIAL DAMAGE ${spDamage}`);
      return spDamage;
    }
    console.log('ZERO DAMAGE');
    return 0;
  }

  /**
   * Calls each function in extraEffects
   * @param {Pokemon} attackingPokemon 
   * @param {Pokemon[]} defendingPokemon 
   * @returns {void}
   */
  handleExtraEffects(attackingPokemon, defendingPokemon) {
    this.extraEffects.forEach(effect => effect(attackingPokemon, defendingPokemon));
  }

  /**
   * @callback Effect
   * @param {Pokemon} user
   * @param {Pokemon[]} targets
   * @returns {void}
   */
  /**
   * @param {Effect} effect A function that is evaluated when a Pokemon fights. 
   * @returns {Skill} this
   */
  addEffect(effect) {
    this.extraEffects.push(effect);
    return this;
  }

}
