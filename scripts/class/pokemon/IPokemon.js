
import { AllPokemon } from '../../lib/Pokemon.lib.js';
import { Direction } from '../enums/Direction.js';
import HeldItem from '../items/HeldItem.js';
import Pokemon from './Pokemon.js';
import Skill from './Skill.js';
import Status from './Status.js';
import Trainer from '../trainer/Trainer.js'
import Util from '../Util.js';
import { BasicMelee, BasicRange } from '../../lib/Skill.lib.js';

/**
 * Defines the base Pokemon template and all its fields
 */
export default class IPokemon {

  /**
   * @interface IPokemon
   */
  constructor() {
    this.id        = 0;
    this.name      = '';
    this.base      = { HP: 0, ATK: 0, DEF: 0, SATK: 0, SDEF: 0, SPE: 0, atkSpeMod: 1, initiativeMod: 400, moveSpeMod: 1, ppGrowthRate: 1 };
    this.current   = { HP: 0, ATK: 0, DEF: 0, SATK: 0, SDEF: 0, SPE: 0, atkSpeMod: 1, initiativeMod: 400, moveSpeMod: 1, ppGrowthRate: 1 };
    this.direction = Direction.W;
    this.level     = 1;
    this.pp        = 0;
    this.rarity    = 1;
    this.skill     = new Skill();
    this.spriteUrl = 'https://img.pokemondb.net/sprites/black-white/normal/unown-qm.png';
    this.types     = ['', ''];
    /**
     * @type {string[]}
     */
    this.evolution = [];
    /**
     * @type {HeldItem}
     */
    this.heldItem;
    /**
     * @type {Trainer}
     */
    this.trainer;
    /**
     * @type {Status[]}
     */
    this.statusCondition = [];
  }

  /**
   * @returns {number} BST = Base Stat Total. The sum of all 6 base stats.
   */
  getBST() {
    return this.base.HP + this.base.ATK + this.base.DEF + this.base.SATK + this.base.SDEF + this.base.SPE;
  }

  /**
   * BST = Base Stat Total. The sum of all 6 base stats
   * @returns {number} A number between 1-7 inclusive (though we should only use 1-5). The higher the BST, the higher the rarity.
   */
  getRarityUsingBST() {
    const bst = this.getBST();
    // Here be starters, HM slaves, and Pokemon so shitty you forget they exist
    if (bst <= 325) return 1;
    // Here be evolved starters, basic Pokemon with 1 evolution, and Little Cup rejects
    else if (bst <= 435) return 2;
    // Here be a potpourri of missed potential, like fully evolved bugs, or Pokemon that can't evolve
    else if (bst < 500) return 3;
    // Here be most fully evolved Pokemon and generally threatening Pokemon to battle against
    else if (bst < 550) return 4;
    // Here be pseudo-legends, legends, and fully evolved Pokemon that may as well be legends
    else if (bst <= 600) return 5;
    // Here be true legends, the strongest Mega Evolved Pokemon, and Slaking
    else if (bst < 720) return 6;
    // Here be actual gods. Fun fact, there are 7 of them if you include megas and alt-forms
    else return 7;
  }

  /**
   * Builds a Pokemon object by synchronously searching Pokemon.lib.js for a Pokemon.
   * Defines their stats, types, and teaches the Pokemon an Attack Skill.
   * @param {Skill} skill determines the Attack Skill this Pokemon will use in Battle
   * @returns {Pokemon} the constructed Pokemon object
   */
  init(skill) {
    this.skill = skill ? skill : this.base.SATK >= this.base.ATK ? BasicRange : BasicMelee;
    const query = this.name ? this.name.toLowerCase() : this.id ? this.id : null;
    if (!query) throw new Error('This Pokemon is missing a name or id');
    const pkmnFromLib = AllPokemon.find(pkmn => (pkmn.name === query || pkmn.id === parseInt(query)));
    if (pkmnFromLib) {
      return Object.assign(new Pokemon(), pkmnFromLib).withSkill(skill);
    }
  }

  /**
   * Builds a Pokemon object by asynchronously querying PokeAPI for a Pokemon.
   * Defines their stats, types, and teaches the Pokemon an Attack Skill.
   * @ignore Use init() instead
   * @param {Skill} skill determines the Attack Skill this Pokemon will use in Battle
   * @returns the constructed Pokemon object
   */
  async query(skill) {
    this.skill = skill ? skill : this.base.SATK >= this.base.ATK ? BasicRange : BasicMelee;
    const query = this.name ? this.name.toLowerCase() : this.id ? this.id : null;
    /**
     * Sample response {@link https://pokeapi.co/api/v2/pokemon/ditto/}
     */
    const pokeAPI = await Util.fetchThenCatch('https://pokeapi.co/api/v2/pokemon/' + query);
    /**
     * @type {number}
     */
    this.id = pokeAPI.id;
    /**
     * @type {string}
     */
    this.name = pokeAPI.name;
    /**
     * Sample response {@link https://pokeapi.co/api/v2/pokemon-species/132}
     */
    const species = await Util.fetchThenCatch(pokeAPI.species.url);
    /**
     * Sample response with no evolution {@link https://pokeapi.co/api/v2/evolution-chain/66}
     * Sample response with many evolutions {@link https://pokeapi.co/api/v2/evolution-chain/67}
     */
    const evoChain = await Util.fetchThenCatch(species.evolution_chain.url);
    /**
     * @TODO Handle weird evolutions
     * @type {Array} of { evolution_details:[], evolves_to:[], is_baby:boolean, species: { name:string, url:string } }
     */
    const evolvesTo = evoChain.chain.evolves_to;
    if (evolvesTo.length === 0) {
      this.evolution = [];
    } else if (evolvesTo.length > 0 && evoChain.chain.species.name === this.name) {
      evolvesTo.forEach(ev => this.evolution.push(ev.species.name)); // I am a basic Pokemon and can evolve
    } else if (evolvesTo[0].evolves_to.length > 0 && evolvesTo[0].species.name === this.name) {
      evolvesTo[0].evolves_to.forEach(ev => this.evolution.push(ev.species.name)); // I am an evolved Pokemon that can evolve
    }
    /**
     * @type {Array} of { base_stat:number, effort:number, stat: { name:string, url:string } }
     */
    const stats = pokeAPI.stats;
    stats.forEach(s => {
      switch (s.stat.name) {
        case 'hp'             : this.base.HP   = s.base_stat; break;
        case 'attack'         : this.base.ATK  = s.base_stat; break;
        case 'defense'        : this.base.DEF  = s.base_stat; break;
        case 'special-attack' : this.base.SATK = s.base_stat; break;
        case 'special-defense': this.base.SDEF = s.base_stat; break;
        case 'speed'          : this.base.SPE  = s.base_stat; break;
        default               : throw new Error('Unrecognized stat from PokeAPI\n' + s);
      }
    });
    this.current = {...this.base};
    this.rarity = this.getRarityUsingBST();
    this.spriteUrl = pokeAPI.sprites.front_default;
    /**
     * @type {Array} of { slot:number, type: { name:string, url:string } }
     */
    const types = pokeAPI.types;
    types.forEach(t => {
      this.types[Number(t.slot) - 1] = t.type.name;
    });
    return this;
  }

}
