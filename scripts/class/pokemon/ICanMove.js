

import IPokemon from './IPokemon.js';

export default class ICanMove extends IPokemon {

  /**
   * @interface ICanMove
   * @augments IPokemon
   */
  constructor() { super(); }

  /**
   * @returns {number}
   */
  getBaseInitiative() {
    return (this.base.initiativeMod - this.base.SPE) / this.base.initiativeMod;
  }

  /**
   * @returns {number}
   */
  getCurrentInitiative() {
    return (this.current.initiativeMod - this.current.SPE) / this.current.initiativeMod;
  }

  /**
   * @TODO Apply some function to this Pokemon's SPE to determine the rate at which this Pokemon moves
   * @returns {number}
   */
  getMovementSpeed() {
    return this.current.SPE * this.current.moveSpeMod;
  }

}
