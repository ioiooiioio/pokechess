import HeldItem from '../items/HeldItem.js';
import ICanFight from './ICanFight.js';
import Skill from './Skill.js';
import Status from './Status.js';
import Trainer from '../trainer/Trainer.js'

/**
 * Defines a Pokemon object with name, stats, types, and a sprite
 */
export default class Pokemon extends ICanFight {

  /**
   * @class
   * @augments ICanFight ICanMove IPokemon
   * @param {string | number} nameOrId A Pokemon's name or dex number
   * @param {Trainer} trainer An identifier for this Pokemon's owner
   */
  constructor(nameOrId, trainer) {
    super();
    this.name    = (typeof nameOrId === 'string') ? nameOrId : '';
    this.id      = (typeof nameOrId === 'number') ? nameOrId : 0;
    this.trainer = trainer;
    /**
     * @type {Status[]}
     */
    this.statusCondition = [];
  }

  /**
   * Should be called regularly during the game loop. Calculates the Pokemon's current stats based on its status conditions.
   * Applies stat changes, deals poison damage, deals burn damage, applies speed debuff from paralysis, maybe wake up from sleep
   * And removes expired status effects
   * @returns {void} void
   */
  applyStatus() {
    const {HP, ...otherStats} = this.base;
    Object.assign(this.current, otherStats);
    this.statusCondition.forEach(status => status.activate(this));
    this.statusCondition = this.statusCondition.filter(status => status.duration !== 0);
    if (this.heldItem) {
      if (this.heldItem.trigger(this)) {
        this.heldItem.effect(this);
      }
    }
  }

  /**
  * Creates a new pokemon as a clone of this one 
  * Note: Status and modifiers are not copied, only base stats
  * @param {Trainer} trainer The owner of this Pokemon. Can be null
  */
  cloneForTrainer(trainer) {
    const newPoke     = new Pokemon(this.name, trainer);
    newPoke.id        = this.id;
    newPoke.base      = {...this.base};
    newPoke.current   = {...this.base};
    newPoke.evolution = this.evolution;
    newPoke.types     = this.types;
    newPoke.skill     = this.skill;
    newPoke.spriteUrl = this.spriteUrl;
    return newPoke;
  }

  /**
   * @param {HeldItem} heldItem Replaces the Pokemon's Held Item
   * @returns {Pokemon} this
   */
  withHeldItem(heldItem) {
    this.heldItem = heldItem;
    return this;
  }

  /**
   * @param {Skill} skill Replaces the Pokemon's Attack Skill
   * @returns {Pokemon} this
   */
  withSkill(skill) {
    this.skill = skill;
    return this;
  }

}
