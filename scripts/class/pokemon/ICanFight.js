import ICanMove from './ICanMove.js';
import Pokemon from './Pokemon.js';
import { BasicMelee, BasicRange } from '../../lib/Skill.lib.js'

export default class ICanFight extends ICanMove {

  /**
   * @interface ICanFight
   * @augments ICanMove
   */
  constructor() { super(); }

  /**
   * @param {Pokemon} targetPokemon Calculates the amount of damage this Pokemon would deal to target Pokemon
   * @returns {number} the amount of damage that would be dealt with the next attack
   */
  calcDamage(targetPokemon) {
    const basicSkill = this.current.SATK >= this.current.ATK ? BasicRange : BasicMelee;
    return (this.skill && this.pp >= this.skill.ppCost)
      ? Math.floor(this.skill.damage(this, targetPokemon))
      : Math.floor(basicSkill.damage(this, targetPokemon));
  }

  /**
   * Determines the optimal attack target. Favors targets this can KO, then the target it deals the most damage to.
   * @param  {...Pokemon} targets A list of potential Pokemon to attack
   * @returns {Pokemon} the chosen Pokemon to attack among targets
   */
  chooseTarget(...targets) {
    if (targets.length < 1) {
      throw new Error('y u do dis');
    } else if (targets.length === 1) {
      return targets[0];
    }
    const targetsICanKO = targets.filter(target => target.current.HP - this.calcDamage(target) <= 0);
    if (targetsICanKO.length < 1) {
      return targets.reduce((a, b) => Math.max(this.calcDamage(a), this.calcDamage(b)));
    } else if (targetsICanKO.length === 1) {
      return targetsICanKO[0]
    }
    return targetsICanKO.reduce((a, b) => Math.max(this.calcDamage(a), this.calcDamage(b)));
  }

  /**
   * @param {...Pokemon} targets This Pokemon deals damage to each target Pokemon
   * @returns the defending Pokemon after taking damage
   */
  fight(...targets) {
    targets.forEach(pkmn => {
      pkmn.current.HP -= this.calcDamage(pkmn);
    });
    if (this.skill && this.pp >= this.skill.ppCost) {
      this.skill.handleExtraEffects(this, targets);
      this.pp -= this.skill.ppCost;
    }
    this.pp += this.current.ppGrowthRate;
    return targets;
  }

  /**
   * @returns The range this Pokemon can attack from this turn
   */
  getAttackRange() {
    const noPP_Skill = this.current.SATK >= this.current.ATK ? BasicRange : BasicMelee;
    return this.pp >= this.skill.ppCost ? this.skill.range : noPP_Skill.range;
  }

  /**
   * @TODO Apply some function to this Pokemon's SPE to determine the rate at which this Pokemon attacks
   * @returns {number}
   */
  getAttackSpeed() {
    return this.spe;
  }

}
