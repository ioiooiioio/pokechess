/**
 * Enum for Skills and what Pokemon the Skill can target
 * @readonly
 * @enum {string}
 */
export const TargetMode = Object.freeze({
  ALLY        : 'ally',          // should return a list of allies within range
  ALLY_BOARD  : 'ally board',    // should return user and all allies on the board
  BOARD       : 'board',         // should return all pokemon on the board
  FOE         : 'foe',           // should return a list of all opponents within range
  FOE_BOARD   : 'foe board',     // should return all enemies on the board
  FOE_OR_ALLY : 'foe or ally',   // should return a list of allies or enemies within range
  USER        : 'user',          // should return the skill caster
  USER_OR_ALLY: 'user or ally'   // should return the skill caster and allies within range
});
