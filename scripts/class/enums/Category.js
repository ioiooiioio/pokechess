/**
 * Enum for Damage Class
 * @readonly
 * @enum {string}
 */
export const Category = Object.freeze({
  PHYS: 'physical',
  SPEC: 'special',
  STAT: 'status',
});
