/**
 * Enum for Fonts
 * @readonly
 * @enum {string | number}
 */
export const Font = Object.freeze({
  STYLE: 'Arial',
  MONOSPACED: 'Monospace',
  SIZE_LG: 8,
  SIZE_MD: 7,
  SIZE_SM: 5.8
});
