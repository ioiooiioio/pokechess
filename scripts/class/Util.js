export default class Util {

  /**
   * @param {string} url The url to fetch from
   * @returns {any} The json from the api response after being parsed to an object
   * @throws {Error} Http error codes of 400 and up
   */
  static async fetchThenCatch(url) {
    try {
      const response = await fetch(url);
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(`[${response.status}] ${response.statusText}\n${response.body}`);
      }
    } catch (err) {
      return console.log('Likely a network issue from a rejected Promise\n' + err);
    }
  }

}
