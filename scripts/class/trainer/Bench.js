import Pokemon from '../pokemon/Pokemon.js';

export default class Bench {

  constructor() {
    /** @type {Pokemon[]} */
    this.pokemon = [];
    /** @type {HTMLImageElement[]} */
    this.pokemonImgs = [];
  }

  /**
   * Refresh the sprites in this.pokemonImgs using this.pokemon
   */
  refreshBenchPkmnImgs() {
    this.pokemonImgs = [];
    this.pokemon.forEach((pkmn, i) => {
      this.pokemonImgs[i] = new Image();
      this.pokemonImgs[i].onerror = (event) => console.error(`Could not find ${this.trainer.wild.pokemonImgs[i].src}\n${event}`);
      this.pokemonImgs[i].src = pkmn.spriteUrl;
    });
  }

  /**
   * Switches the location of two pokemon on your bench
   * @param {number} index1 
   * @param {number} index2 
   */
  swapPokemon(index1, index2) {
    const pkmn1 = this.pokemon[index1];
    const pkmn2 = this.pokemon[index2];
    this.pokemon.splice(index1, 1, pkmn2);
    this.pokemon.splice(index2, 1, pkmn1);
  }

}
