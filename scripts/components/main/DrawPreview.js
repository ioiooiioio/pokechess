
import { Font } from '../../class/enums/Font.js';
import ICanvas from '../ICanvas.js';
import Rect from '../Rect.js';
import Trainer from '../../class/trainer/Trainer.js';

export default class DrawPreview extends ICanvas {

  /**
   * @class
   * @param {Trainer} trainer The current player
   */
  constructor(trainer) {
    super();
    this.trainer = trainer;
    /** This is the rect of the preview zone measured as a percentage of the screen */
    this.zoneRect = new Rect(0.75, 0.05, 0.25, 0.65);
  }

  update() {
    this.context.clearRect(...this.rect);
    this.rect.setAll(
      this.zoneRect.x * this.canvas.width,
      this.zoneRect.y * this.canvas.height,
      this.zoneRect.w * this.canvas.width,
      this.zoneRect.h * this.canvas.height);
    this.context.fillStyle = 'rgba(50, 50, 50, 0.2)';
    this.context.fillRect(...this.rect);

    this.context.font = `30px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    this.context.textAlign = 'center';
    this.context.fillText('Preview', this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2);
  }

}
