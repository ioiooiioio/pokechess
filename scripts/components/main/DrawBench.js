
import { Font } from '../../class/enums/Font.js';
import ICanvas from '../ICanvas.js';
import Point from '../Point.js';
import Pokemon from '../../class/pokemon/Pokemon.js';
import Rect from '../Rect.js';
import Trainer from '../../class/trainer/Trainer.js';

export default class DrawBench extends ICanvas {

  /**
   * @class
   * @param {Trainer} trainer The current player
   */
  constructor(trainer) {
    super();
    this.trainer           = trainer;
    this.benchCol          = 8;           // The number of columns in the bench
    this.benchRow          = 2;           // The number of rows in the bench
    this.clickedBenchIndex = -1;          // The index of the last pokemon you clicked on the bench
    this.clickedBenchRect  = new Rect();  // The Rect that the clicked pokemon sprite lives in
    this.imgRect           = new Rect();  // The Rect that benched pokemon sprites live in
    this.chartRect         = new Rect();  // The Rect that the radar chart lives in
    this.tooltipRect       = new Rect();  // The Rect that tooltips live in
    /** @type {Rect[][]} Contains each of the smaller Rects that benched pokemon sprites live in */
    this.benchRects = [];
    for (let row = 0; row < this.benchRow; row++) {
      this.benchRects[row] = [];
      for (let col = 0; col < this.benchCol; col++) {
        this.benchRects[row][col] = new Rect();
      }
    }
    /** This is the rect of the bench zone measured as a percentage of the screen */
    this.zoneRect = new Rect(0.2, 0.7, 0.55, 0.3);

    this.canvas.addEventListener('mousedown', event => this.onClick(event));
  }

  drawBenchedPokemon() {
    this.benchRects.forEach(col => col.forEach(row => this.context.clearRect(...row)));
    // Draw inventory item table from left to right, top to bottom
    for (let row = 0; row < this.benchRow; row++) {
      for (let col = 0; col < this.benchCol; col++) {
        this.benchRects[row][col].setAll(
          // 1st set of parens is the left padding. 2nd set of parens is space between each rect.
          this.rect.x + (this.rect.w * 0.015) + (this.rect.w / this.benchCol * col * 0.98),
          this.rect.y + (this.rect.h * 0.06) + (this.rect.h * 0.46 * row),
          this.rect.w / this.benchCol * 0.9,   // width of table cell
          this.rect.w / this.benchCol * 0.9);  // height of table cell
        this.context.fillStyle = 'rgba(0, 0, 100, 0.3)';
        this.context.fillRect(...this.benchRects[row][col]);
        // Draw benched pokemon sprite
        const i = row * this.benchCol + col;
        const pokemon = this.trainer.bench.pokemon[i];
        if (pokemon) {
          const pokemonImgW = this.trainer.bench.pokemonImgs[i].width * this.scaleRatio() * 0.4;
          const pokemonImgH = this.trainer.bench.pokemonImgs[i].height * this.scaleRatio() * 0.4;
          this.context.drawImage(this.trainer.bench.pokemonImgs[i],
            this.benchRects[row][col].x + (this.benchRects[row][col].w * 0.5) - (pokemonImgW * 0.5),
            /* This line puts the pokemon in the center of the rect. */
            this.benchRects[row][col].y + (this.benchRects[row][col].h * 0.5) - (pokemonImgH * 0.5),
            /* Alternatively, this line puts the pokemon on the 'floor' of the rect. */
            // this.canvas.height * this.zoneRect.y + this.benchRects[row][col].h * 1.1 - pokemonImgH,
            pokemonImgW, pokemonImgH);
        }
      }
    }
  }

  /**
   * Attaches the sprite of the clicked pokemon to your cursor. It goes away when you click something else.
   */
  drawClickedBenchPkmn() {
    // Draw clicked Pokemon
    if (this.clickedBenchIndex < 0) { return; }
    this.clickedBenchRect.setAll(
      this.lastMousePos.x - this.trainer.bench.pokemonImgs[this.clickedBenchIndex].width * 0.6,
      this.lastMousePos.y - this.trainer.bench.pokemonImgs[this.clickedBenchIndex].height * 0.5,
      this.trainer.bench.pokemonImgs[this.clickedBenchIndex].width * this.scaleRatio() * 0.44,
      this.trainer.bench.pokemonImgs[this.clickedBenchIndex].height * this.scaleRatio() * 0.44);
    this.context.drawImage(this.trainer.bench.pokemonImgs[this.clickedBenchIndex], ...this.clickedBenchRect);
    // Get cursor position
    const mouseIsOverBenchIndex = this.lastMousePos.indexOfBoundingRect(...this.benchRects.flat());
    if (mouseIsOverBenchIndex >= 0) { return; }
    // If cursor is over a bench pokemon and is holding a bench pokemon, draw comparison tooltip
  }

  /**
   * A hexagonal radar chart representing this Pokemon's stats.
   * @param {Pokemon} pokemon Draws this Pokemon's base stats inside the tooltip
   */
  drawStatChart(pokemon) {
    const radius       = 0.85 * this.chartRect.w / 2;
    const center       = new Point(this.chartRect.x + this.chartRect.w / 2, this.chartRect.y + this.chartRect.h / 2);
    const statAtRadius = 150;
    const root3over2   = Math.sqrt(3) / 2;
    // Draw outer circle
    this.context.strokeStyle = 'rgba(255, 255, 255, 1)';
    this.context.beginPath();
    this.context.ellipse(center.x, center.y, radius, radius, 0, 0, 2 * Math.PI);
    this.context.stroke();
    // Draw inner hexagon
    this.context.beginPath();
    this.context.moveTo(center.x, center.y - radius * pokemon.base.HP / statAtRadius);
    this.context.lineTo(center.x + root3over2 * radius * pokemon.base.DEF / statAtRadius, center.y - 1 / 2 * radius * pokemon.base.DEF / statAtRadius);
    this.context.lineTo(center.x + root3over2 * radius * pokemon.base.ATK / statAtRadius, center.y + 1 / 2 * radius * pokemon.base.ATK / statAtRadius);
    this.context.lineTo(center.x, center.y + radius * pokemon.base.SPE / statAtRadius);
    this.context.lineTo(center.x - root3over2 * radius * pokemon.base.SATK / statAtRadius, center.y + 1 / 2 * radius * pokemon.base.SATK / statAtRadius);
    this.context.lineTo(center.x - root3over2 * radius * pokemon.base.SDEF / statAtRadius, center.y - 1 / 2 * radius * pokemon.base.SDEF / statAtRadius);
    this.context.lineTo(center.x, center.y - radius * pokemon.base.HP / statAtRadius);
    // Fill hexagon with gradient
    const gradient1 = this.context.createLinearGradient(
      0, this.chartRect.y,
      0, this.chartRect.y + this.chartRect.h);
    const gradient2 = this.context.createLinearGradient(
      this.chartRect.x, this.chartRect.y + this.chartRect.h * (1 - root3over2),
      this.chartRect.x + this.chartRect.w, this.chartRect.y + this.chartRect.h * root3over2);
    const gradient3 = this.context.createLinearGradient(
      this.chartRect.x + this.chartRect.w, this.chartRect.y + this.chartRect.h * (1 - root3over2),
      this.chartRect.x, this.chartRect.y + this.chartRect.h * root3over2);
    [gradient1, gradient2, gradient3].forEach(gradient => {
      gradient.addColorStop(0, 'rgba(50, 255, 50, 0.8)');
      gradient.addColorStop(0.5, 'rgba(255, 255, 255, 0.1)');
      gradient.addColorStop(1, 'rgba(255, 50, 50, 0.8)');
      this.context.fillStyle = gradient;
      this.context.fill();
    });
    this.context.stroke();
    // Draw 6 radius lines and labels
    this.context.fillStyle = 'rgba(255, 255, 255, 1)';
    const tangentPoint = new Point();
    [
      [0, 1, 'SPE'], [root3over2, 0.5, 'ATK'], [root3over2, -0.5, 'DEF'],
      [0, -1, 'HP'], [-root3over2, -0.5, 'SDEF'], [-root3over2, 0.5, 'SATK']
    ].forEach(p => {
      this.context.strokeStyle = 'rgba(255, 255, 255, 1)';
      this.context.lineWidth = 1;
      this.context.beginPath();
      this.context.moveTo(...center);
      tangentPoint.setAll(center.x + radius * p[0], center.y + radius * p[1]);
      this.context.lineTo(...tangentPoint);
      this.context.stroke();
      // Draw stat label
      tangentPoint.setAll(center.x + 1.2 * radius * p[0], 1.01 * center.y + 1.1 * radius * p[1]);
      this.context.strokeStyle = 'rgba(0, 0, 0, 1)';
      this.context.lineWidth = 3;
      this.context.strokeText(p[2], ...tangentPoint);
      this.context.fillText(p[2], ...tangentPoint);
    });
  }

  /**
   * Draws a tooltip after mouseover on a benchRect for secondsToDrawTooltip seconds
   */
  drawTooltip() {
    if (this.framesSinceCursorMoved < this.secondsToDrawTooltip * this.frameRate) {
      // TODO use this time to animate tooltip growing from 0% to 100%
      this.framesSinceCursorMoved++;
    } else {
      const benchRectArr = this.benchRects.flat();
      const mouseIsOverBenchIndex = this.lastMousePos.indexOfBoundingRect(...benchRectArr);
      // if mouse is over bench pkmn and not holding a bench pkmn, draw Tooltip
      if (mouseIsOverBenchIndex >= 0 && this.clickedBenchIndex < 0
        && mouseIsOverBenchIndex < this.trainer.bench.pokemon.length) {
        const pokemon = this.trainer.bench.pokemon[mouseIsOverBenchIndex];
        const type0 = `${this.titleCase(pokemon.types[0])}`;
        const type1 = `${this.titleCase(pokemon.types[1])}`;
        const stats = [
          `  HP: ${pokemon.base.HP}`,
          ` ATK: ${pokemon.base.ATK}`,
          ` DEF: ${pokemon.base.DEF}`,
          `SATK: ${pokemon.base.SATK}`,
          `SDEF: ${pokemon.base.SDEF}`,
          ` SPE: ${pokemon.base.SPE}`,
          `Total ${pokemon.getBST()}`
        ];
        this.context.font = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.MONOSPACED}`; // Font size affects textWidth
        const tooltipW = this.context.measureText(stats[6]).width * 4.1;
        const tooltipH = Font.SIZE_SM * this.scaleRatio() * (type1 ? 15.0 : 13.6);
        // Draw tooltip rect
        this.tooltipRect.setAll(this.lastMousePos.x, this.lastMousePos.y - tooltipH, tooltipW, tooltipH);
        this.context.fillStyle = 'rgba(0, 0, 0, 0.7)';
        this.context.fillRect(...this.tooltipRect);
        // Draw pokemon type
        this.context.textAlign = 'center';
        this.context.fillStyle = 'rgba(255, 255, 255, 1)';
        const xPosition = this.lastMousePos.x + this.benchRects[0][0].w * 0.5;
        this.context.fillText(this.titleCase(pokemon.name), xPosition,
          this.lastMousePos.y - this.tooltipRect.h * 0.9);
        const textHeight = Font.SIZE_SM * this.scaleRatio() * 1.4;
        this.context.fillText(type0, xPosition,
          this.lastMousePos.y - this.tooltipRect.h * 0.9 + textHeight);
        if (type1) {
          this.context.fillText(type1, xPosition,
            this.lastMousePos.y - this.tooltipRect.h * 0.9 + 2 * textHeight);
        }
        // Draw pokemon stats
        stats.forEach((stat, i) => {
          let paddingTop = (type1 ? 1.3 : 1.3) - (i * 0.2);
          if (stat.includes('Total')) { paddingTop += 0.03; }
          this.context.fillText(stat, xPosition,
            this.lastMousePos.y - this.benchRects[0][0].h * paddingTop);
        });
        // Draw Stat Chart Rect
        this.chartRect.setAll(
          this.tooltipRect.x + this.tooltipRect.w * 0.37,
          this.tooltipRect.y + this.tooltipRect.h * 0.05,
          this.tooltipRect.w * 0.6,
          this.tooltipRect.w * 0.6);
        this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
        this.context.fillRect(...this.chartRect);
        this.drawStatChart(pokemon);
      }
    }
  }

  /**
   * @param {MouseEvent} event From Event Listener
   */
  onClick(event) {
    const canvasRect          = this.canvas.getBoundingClientRect();
    const clickPos            = new Point(event.clientX - canvasRect.left, event.clientY - canvasRect.top);
    const indexOfPkmnOnCursor = this.clickedBenchIndex;
    this.clickedBenchIndex    = clickPos.indexOfBoundingRect(...this.benchRects.flat());
    // if an empty bench pkmn rect is clicked, do nothing
    if (!this.trainer.bench.pokemon[this.clickedBenchIndex]) {
      this.clickedBenchIndex = -1;
      return;
    }
    // if holding a bench pokemon and user clicks on a bench pkmn again, swap the locations of the two pokemon
    if (indexOfPkmnOnCursor >= 0 && this.clickedBenchIndex >= 0 && this.clickedBenchIndex < this.trainer.bench.pokemon.length) {
      // Refresh the sprites in the bench zone
      this.trainer.bench.swapPokemon(indexOfPkmnOnCursor, this.clickedBenchIndex);
      this.trainer.bench.refreshBenchPkmnImgs();
      this.clickedBenchIndex = -1;
    } else if (indexOfPkmnOnCursor >= 0) {
      // remove the pokemon attached to the cursor
      this.clickedBenchIndex = -1;
    }
    console.log(`pickUpBenchPkmn[${this.clickedBenchIndex}] clickPos(${clickPos.x}, ${clickPos.y})`);
  }

  update() {
    this.context.clearRect(...this.rect);
    this.rect.setAll(
      this.zoneRect.x * this.canvas.width,
      this.zoneRect.y * this.canvas.height,
      this.zoneRect.w * this.canvas.width,
      this.zoneRect.h * this.canvas.height);
    this.context.fillStyle = 'rgba(0, 0, 100, 0.2)';
    this.context.fillRect(...this.rect);

    this.drawBenchedPokemon();
    this.drawTooltip();
    this.updateFPS(); // inherited from super class

    this.context.font = `30px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    this.context.textAlign = 'center';
    this.context.fillText('Bench', this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2);
  }

}
