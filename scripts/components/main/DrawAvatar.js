
import { Font } from '../../class/enums/Font.js';
import ICanvas from '../ICanvas.js';
import Point from '../Point.js';
import Rect from '../Rect.js';
import Trainer from '../../class/trainer/Trainer.js';

export default class DrawAvatar extends ICanvas {
  /**
   * @class
   * @param {Trainer} trainer The current player
   */
  constructor(trainer) {
    super();
    this.trainer         = trainer;
    this.buyExpRect      = new Rect();  // The Rect that Buy Exp button lives in
    this.refreshShopRect = new Rect();  // The Rect that Refresh shop button lives in
    this.tooltipRect     = new Rect();  // The Rect that tooltipText lives in
    this.canvas.addEventListener('mousedown', event => this.onClick(event));
    /** This is the rect of the avatar zone measured as a percentage of the screen */
    this.zoneRect = new Rect(0, 0.75, 0.2, 0.25);
  }

  drawBuyExpButton() {
    this.buyExpRect.setAll(
      this.rect.w * 0.7,
      this.rect.h * 0.75 + this.rect.y,
      this.rect.w * 0.27,
      this.rect.h * 0.2);
    this.context.fillStyle = 'rgba(100, 100, 0, 0.3)';
    this.context.fillRect(...this.buyExpRect);
    this.context.fillStyle = 'rgba(0, 0, 0, 1)';
    this.context.font = `${Font.SIZE_MD * this.scaleRatio()}px ${Font.STYLE}`;
    this.context.textAlign = 'center';
    this.context.fillText('Buy Exp',
      this.buyExpRect.w * 0.5 + this.buyExpRect.x,
      this.buyExpRect.h * 0.64 + this.buyExpRect.y);
  }

  drawRefreshShopButton() {
    this.refreshShopRect.setAll(
      this.rect.w * 0.03,
      this.rect.h * 0.75 + this.rect.y,
      this.rect.w * 0.27,
      this.rect.h * 0.2);
    this.context.fillStyle = 'rgba(100, 100, 0, 0.3)';
    this.context.fillRect(...this.refreshShopRect);
    this.context.fillStyle = 'rgba(0, 0, 0, 1)';
    this.context.font = `${Font.SIZE_MD * this.scaleRatio()}px ${Font.STYLE}`;
    this.context.textAlign = 'center';
    this.context.fillText('♻ Shop',
      this.refreshShopRect.w * 0.5 + this.refreshShopRect.x,
      this.refreshShopRect.h * 0.64 + this.refreshShopRect.y);
  }

  drawTooltip() {
    const mouseIsOverButton = this.lastMousePos.indexOfBoundingRect(this.buyExpRect, this.refreshShopRect);
    if (mouseIsOverButton === 0) {
      // if mouse is over buy exp rect, draw Tooltip
      this.context.font      = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.STYLE}`;
      this.context.textAlign = 'center';
      // Draw tooltip rect
      const tooltipText = '💰 3 for 3 🍬';
      const textWidth   = this.context.measureText(tooltipText).width + 20;
      const textHeight  = this.buyExpRect.h;
      this.tooltipRect.setAll(
        this.lastMousePos.x - textWidth * 0.5,
        this.lastMousePos.y - textHeight,
        textWidth,
        textHeight);
      this.context.fillStyle = 'rgba(0, 0, 0, 0.7)';
      this.context.fillRect(...this.tooltipRect);
      // Draw tooltip text
      this.context.fillStyle = 'rgba(255, 255, 255, 1)';
      this.context.fillText(tooltipText,
        this.lastMousePos.x + textWidth * 0.01,
        this.lastMousePos.y - textHeight * 0.4);
    } else if (mouseIsOverButton === 1) {
      // if mouse is over refresh shop rect, draw Tooltip
      this.context.font      = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.STYLE}`;
      this.context.textAlign = 'center';
      // Draw tooltip rect
      const tooltipText = `💰${this.trainer.shop.refreshCost} to refresh`;
      const textWidth = this.context.measureText(tooltipText).width + 20;
      const textHeight = this.buyExpRect.h;
      this.tooltipRect.setAll(
        this.lastMousePos.x - textWidth * 0.5,
        this.lastMousePos.y - textHeight,
        textWidth,
        textHeight);
      this.context.fillStyle = 'rgba(0, 0, 0, 0.7)';
      this.context.fillRect(...this.tooltipRect);
      // Draw tooltip text
      this.context.fillStyle = 'rgba(255, 255, 255, 1)';
      this.context.fillText(tooltipText,
        this.lastMousePos.x + textWidth * 0.01,
        this.lastMousePos.y - textHeight * 0.4);
    }
  }

  drawTrainerInfo() {
    this.context.fillStyle = 'rgba(0, 0, 0, 1)';
    this.context.font = `${Font.SIZE_MD * this.scaleRatio()}px ${Font.STYLE}`;
    // Draw Trainer level
    this.context.textAlign = 'center';
    this.context.fillText('⭐'.repeat(this.trainer.level),
      this.rect.x + this.rect.w * 0.5,
      this.rect.y + this.rect.h * 0.15);
    // Draw Trainer currency
    this.context.textAlign = 'left';
    this.context.fillText(`💰 ${this.trainer.currency}`,
      this.rect.x + this.rect.w * 0.03,
      this.rect.y + this.rect.h * 0.15);
    // Draw Trainer exp
    this.context.textAlign = 'right';
    this.context.fillText(`${this.trainer.exp} 🍬`,
      this.rect.x + this.rect.w * 0.97,
      this.rect.y + this.rect.h * 0.15);
    // Draw Trainer exp to next level
    this.context.fillText(`${this.trainer.getExpTNL()} 🍬 to 🌟`,
      this.rect.x + this.rect.w * 0.97,
      this.rect.y + this.rect.h * 0.3);
  }

  /**
   * @param {MouseEvent} event From Event Listener
   */
  onClick(event) {
    const canvasRect = this.canvas.getBoundingClientRect();
    const clickPos   = new Point(event.clientX - canvasRect.left, event.clientY - canvasRect.top);
    const clickedButtonIndex = clickPos.indexOfBoundingRect(this.buyExpRect, this.refreshShopRect);
    if (clickedButtonIndex === 0) {
      this.trainer.buy3Exp();
    } else if (clickedButtonIndex === 1) {
      this.trainer.buyShopRefresh();
      this.trainer.shop.populateShopItemImgs();
    }
  }

  update() {
    this.context.clearRect(...this.rect);
    this.rect.setAll(
      this.zoneRect.x * this.canvas.width,
      this.zoneRect.y * this.canvas.height,
      this.zoneRect.w * this.canvas.width,
      this.zoneRect.h * this.canvas.height);
    this.context.fillStyle = 'rgba(100, 100, 0, 0.2)';
    this.context.fillRect(...this.rect);

    this.drawTrainerInfo();
    this.drawBuyExpButton();
    this.drawRefreshShopButton();
    this.drawTooltip();
    // placeholder text
    this.context.font = `30px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    this.context.textAlign = 'center';
    this.context.fillText(this.trainer.name, this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2);
  }

}
