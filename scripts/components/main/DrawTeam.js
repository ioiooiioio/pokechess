
import { Font } from '../../class/enums/Font.js';
import ICanvas from '../ICanvas.js';
import Rect from '../Rect.js';
import Hex from '../Hex.js';
import Trainer from '../../class/trainer/Trainer.js';

export default class DrawTeam extends ICanvas {

  /**
   * @class
   * @param {Trainer} trainer The current player
   */
  constructor(trainer) {
    super();
    this.trainer = trainer;
    /** This is the rect of the team zone measured as a percentage of the screen */
    this.zoneRect = new Rect(0.2, 0.2, 0.55, 0.5);
  }

  update() {
    this.context.clearRect(...this.rect);
    this.rect.setAll(
      this.zoneRect.x * this.canvas.width,
      this.zoneRect.y * this.canvas.height,
      this.zoneRect.w * this.canvas.width,
      this.zoneRect.h * this.canvas.height);
    this.context.fillStyle = 'rgba(100, 0, 0, 0.2)';
    this.context.fillRect(...this.rect);
    
    this.context.fillStyle = '#ffffff';
    for (let i=0;i<3;i++) {
      for (let j=0;j<3;j++) {
        var hexagon = new Hex(this.rect.x+this.rect.w/2+(i+(j%2==1?0.5:0))*this.rect.w/10,this.rect.y+this.rect.y/2+j*this.rect.h/10,this.rect.w/10,this.rect.h/(5*Math.sqrt(3))+2);
        hexagon.draw(this.context, true)
      }
    }
    

    this.context.font = `30px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    this.context.textAlign = 'center';
    this.context.fillText('Team', this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2);
  }

}
