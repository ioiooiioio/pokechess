
import { Font } from '../../class/enums/Font.js';
import ICanvas from '../ICanvas.js';
import Point from '../Point.js';
import { PokeBall } from '../../lib/Item.lib.js';
import Pokemon from '../../class/pokemon/Pokemon.js';
import Rect from '../Rect.js';
import Trainer from '../../class/trainer/Trainer.js';
import { Type } from '../../class/enums/Type.js';

export default class DrawWild extends ICanvas {

  /**
   * @class
   * @param {Trainer} trainer The current player
   */
  constructor(trainer) {
    super();
    this.trainer              = trainer;
    this.clickedWildPkmnIndex = -1;          // The index of the Wild Pokemon you clicked on
    this.clickedWildPkmnRect  = new Rect();  // The Rect that the clicked pokemon sprite lives in
    this.chartRect            = new Rect();  // The Rect that the radar chart lives in
    this.refreshWildRect      = new Rect();  // The Rect that Refresh Wild button lives in
    this.wildPkmnCount        = 6;           // The number of wild pokemon in the wild
    this.tooltipRect          = new Rect();  // The Rect that the tooltip text lives in

    this.canvas.addEventListener('mousedown', event => this.onClick(event));

    /** @type {Rect[]} The rects that the Wild Pokemon sprites live in */
    this.wildPkmnRects = [];
    for (let i = 0; i < this.wildPkmnCount; i++) {
      this.wildPkmnRects[i] = new Rect();
    }
    /** This is the rect of the wild zone measured as a percentage of the screen */
    this.zoneRect = new Rect(0.2, 0, 0.55, 0.2);
  }

  /**
   * Attaches the sprite of the clicked pokemon to your cursor. It goes away when you click something else.
   */
  drawClickedWildPkmn() {
    // Draw clicked Pokemon
    if (this.clickedWildPkmnIndex < 0) { return; }
    this.clickedWildPkmnRect.setAll(
      this.lastMousePos.x - this.trainer.wild.pokemonImgs[this.clickedWildPkmnIndex].width * 0.6,
      this.lastMousePos.y - this.trainer.wild.pokemonImgs[this.clickedWildPkmnIndex].height * 0.5,
      this.trainer.wild.pokemonImgs[this.clickedWildPkmnIndex].width * this.scaleRatio() * 0.44,
      this.trainer.wild.pokemonImgs[this.clickedWildPkmnIndex].height * this.scaleRatio() * 0.44);
    this.context.drawImage(this.trainer.wild.pokemonImgs[this.clickedWildPkmnIndex], ...this.clickedWildPkmnRect);
    // Get cursor position
    const mouseIsOverWildPkmnIndex = this.lastMousePos.indexOfBoundingRect(...this.wildPkmnRects);
    if (mouseIsOverWildPkmnIndex >= 0) { return; }
    // If cursor is not over a wild pokemon and is holding a wild pokemon, draw additional tooltip
    const preferredPokeball = this.trainer.choosePokeball(this.trainer.wild.pokemon[this.clickedWildPkmnIndex]);
    // TODO What should happen if you have more than 1 ball that can catch the pokemon?
    const tooltipText = !preferredPokeball
      ? 'A better Pokeball is needed'
      : preferredPokeball === PokeBall
        ? 'Use Poke Ball 💰 -1'
        : `Use ${preferredPokeball.name}`;
    const tooltipText2 = !preferredPokeball
      ? 'to catch this Pokemon'
      : preferredPokeball === PokeBall
        ? 'Amount Owned: ∞'
        : `Amount Owned: ${this.trainer.inventory.amountOwned(preferredPokeball)}`;
    const textWidth = this.context.measureText(tooltipText).width * 0.7;
    const textHeight = Font.SIZE_SM * this.scaleRatio() * 3.5;
    // Draw tooltip rect
    this.tooltipRect.setAll(
      this.lastMousePos.x - textWidth * 0.5,
      this.lastMousePos.y - textHeight * 1.4,
      textWidth,
      textHeight);
    this.context.fillStyle = 'rgba(0, 0, 0, 0.7)';
    this.context.fillRect(...this.tooltipRect);
    // Draw tooltip text
    this.context.fillStyle = 'rgba(255, 255, 255, 1)';
    this.context.font = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.STYLE}`;
    this.context.textAlign = 'center'
    this.context.fillText(tooltipText,
      this.lastMousePos.x,
      this.lastMousePos.y - this.wildPkmnRects[0].h * 0.5);
    this.context.fillText(tooltipText2,
      this.lastMousePos.x,
      this.lastMousePos.y - this.wildPkmnRects[0].h * 0.3);
  }

  drawRefreshWildButton() {
    this.refreshWildRect.setAll(
      this.rect.x + this.rect.w * 0.86,
      this.rect.y + this.rect.h * 0.6,
      this.rect.w * 0.1,
      this.rect.h * 0.3);
    this.context.fillStyle = 'rgba(150, 0, 100, 0.4)';
    this.context.fillRect(...this.refreshWildRect);
    this.context.fillStyle = 'rgba(0, 0, 0, 1)';
    this.context.font = `${Font.SIZE_MD * this.scaleRatio()}px ${Font.STYLE}`;
    this.context.textAlign = 'center';
    this.context.fillText('♻ Wild',
      this.refreshWildRect.w * 0.5 + this.refreshWildRect.x,
      this.refreshWildRect.h * 0.64 + this.refreshWildRect.y);
  }

  /**
   * Draws stuff in the Wild zone
   */
  drawWildPokemon() {
    // truncate wildRects if player captures pokemon
    if (this.wildPkmnRects.length > this.trainer.wild.pokemon.length) {
      this.wildPkmnRects.length = this.trainer.wild.pokemon.length;
    }
    // append more Rects if player refreshes wild
    while (this.wildPkmnRects.length < this.trainer.wild.pokemon.length) {
      this.wildPkmnRects.push(new Rect());
    }
    for (let i = 0; i < this.trainer.wild.pokemon.length; i++) {
      this.context.clearRect(...this.wildPkmnRects[i]);
      // Draw wild rect
      this.wildPkmnRects[i].setAll(
        // 1st set of parens is the left padding. 2nd set of parens is space between each rect.
        this.rect.x + (this.rect.w * 0.06) + (this.rect.w * i * 0.8 / this.wildPkmnCount),
        this.rect.y + this.rect.h * 0.05,
        this.rect.w / this.wildPkmnCount * 0.63,
        this.rect.w / this.wildPkmnCount * 0.63);
      this.context.fillStyle = 'rgba(150, 0, 100, 0.4)';
      this.context.fillRect(...this.wildPkmnRects[i]);
      // Draw pokemon sprite
      const pokemonImgW = this.trainer.wild.pokemonImgs[i].width * this.scaleRatio() * 0.4;
      const pokemonImgH = this.trainer.wild.pokemonImgs[i].height * this.scaleRatio() * 0.4;
      this.context.drawImage(this.trainer.wild.pokemonImgs[i],
        // centers the pokemon sprite in wildPkmnRects
        this.wildPkmnRects[i].x + (this.wildPkmnRects[i].w * 0.5) - (pokemonImgW * 0.5),
        /* This line puts the pokemon in the center of the rect. */
        this.wildPkmnRects[i].y + (this.wildPkmnRects[i].h * 0.5) - (pokemonImgH * 0.5),
        /* Alternatively, this line puts the pokemon on the 'floor' of the rect. */
        // this.rect.h * 0.65 - pokemonImgH,
        pokemonImgW, pokemonImgH);
      // Draw rarity
      this.context.fillStyle = 'rgba(0, 0, 0, 1)';
      this.context.font = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.STYLE}`;
      this.context.textAlign = 'center';
      this.context.fillText('⭐'.repeat(this.trainer.wild.pokemon[i].rarity),
        this.wildPkmnRects[i].x + this.wildPkmnRects[i].w * 0.5,
        this.rect.h * 0.75);
      // Draw pokemon name
      this.context.font = `${Font.SIZE_LG * this.scaleRatio()}px ${Font.STYLE}`;
      this.context.textAlign = 'center';
      let pkmnName = this.trainer.wild.pokemon[i].name;
      pkmnName = pkmnName.charAt(0).toUpperCase() + pkmnName.slice(1);
      this.context.fillText(pkmnName,
        this.wildPkmnRects[i].x + this.wildPkmnRects[i].w * 0.5,
        this.rect.h * 0.9);
    }
    // Draw empty wild pkmn rect in place of captured pokemon
    for (let i = this.wildPkmnCount - 1; i >= this.trainer.wild.pokemon.length; i--) {
      const placeholderRect = new Rect(
        this.rect.x + (this.rect.w * 0.06) + (this.rect.w * i * 0.8 / this.wildPkmnCount),
        this.rect.y + this.rect.h * 0.05,
        this.rect.w / this.wildPkmnCount * 0.63,
        this.rect.w / this.wildPkmnCount * 0.63);
      this.context.fillStyle = 'rgba(150, 0, 100, 0.2)';
      this.context.fillRect(...placeholderRect);
    }
  }

  /**
   * A hexagonal radar chart representing this Pokemon's stats.
   * @param {Pokemon} pokemon Draws this Wild Pokemon's base stats
   */
  drawStatChart(pokemon) {
    const radius = 0.85 * this.chartRect.w / 2;
    const center = new Point(this.chartRect.x + this.chartRect.w / 2, this.chartRect.y + this.chartRect.h / 2);
    const statAtRadius = 150;
    const root3over2 = Math.sqrt(3) / 2;
    // Draw hexagon and circle
    this.context.strokeStyle = 'rgba(255, 255, 255, 1)';
    this.context.beginPath();
    this.context.ellipse(center.x, center.y, radius, radius, 0, 0, 2 * Math.PI);
    this.context.stroke();
    this.context.beginPath();
    this.context.moveTo(center.x, center.y - radius * pokemon.base.HP / statAtRadius);
    this.context.lineTo(center.x + root3over2 * radius * pokemon.base.DEF / statAtRadius, center.y - 1 / 2 * radius * pokemon.base.DEF / statAtRadius);
    this.context.lineTo(center.x + root3over2 * radius * pokemon.base.ATK / statAtRadius, center.y + 1 / 2 * radius * pokemon.base.ATK / statAtRadius);
    this.context.lineTo(center.x, center.y + radius * pokemon.base.SPE / statAtRadius);
    this.context.lineTo(center.x - root3over2 * radius * pokemon.base.SATK / statAtRadius, center.y + 1 / 2 * radius * pokemon.base.SATK / statAtRadius);
    this.context.lineTo(center.x - root3over2 * radius * pokemon.base.SDEF / statAtRadius, center.y - 1 / 2 * radius * pokemon.base.SDEF / statAtRadius);
    this.context.lineTo(center.x, center.y - radius * pokemon.base.HP / statAtRadius);
    // Fill hexagon with gradient
    const gradient1 = this.context.createLinearGradient(
      0, this.chartRect.y,
      0, this.chartRect.y + this.chartRect.h);
    const gradient2 = this.context.createLinearGradient(
      this.chartRect.x, this.chartRect.y + this.chartRect.h * (1 - root3over2),
      this.chartRect.x + this.chartRect.w, this.chartRect.y + this.chartRect.h * root3over2);
    const gradient3 = this.context.createLinearGradient(
      this.chartRect.x + this.chartRect.w, this.chartRect.y + this.chartRect.h * (1 - root3over2),
      this.chartRect.x, this.chartRect.y + this.chartRect.h * root3over2);
    [gradient1, gradient2, gradient3].forEach(gradient => {
      gradient.addColorStop(0, 'rgba(50, 255, 50, 0.8)');
      gradient.addColorStop(0.5, 'rgba(255, 255, 255, 0.1)');
      gradient.addColorStop(1, 'rgba(255, 50, 50, 0.8)');
      this.context.fillStyle = gradient;
      this.context.fill();
    });
    this.context.stroke();
    // Draw 6 radius lines and labels
    this.context.fillStyle = 'rgba(255, 255, 255, 1)';
    const tangentPoint = new Point();
    [
      [0, 1, 'SPE'], [root3over2, 0.5, 'ATK'], [root3over2, -0.5, 'DEF'],
      [0, -1, 'HP'], [-root3over2, -0.5, 'SDEF'], [-root3over2, 0.5, 'SATK']
    ].forEach(p => {
      this.context.strokeStyle = 'rgba(255, 255, 255, 1)';
      this.context.lineWidth = 1;
      this.context.beginPath();
      this.context.moveTo(...center);
      tangentPoint.setAll(center.x + radius * p[0], center.y + radius * p[1]);
      this.context.lineTo(...tangentPoint);
      this.context.stroke();
      // Draw stat label
      tangentPoint.setAll(center.x + 1.2 * radius * p[0], 1.03 * center.y + 1.1 * radius * p[1]);
      this.context.strokeStyle = 'rgba(0, 0, 0, 1)';
      this.context.lineWidth = 3;
      this.context.strokeText(p[2], ...tangentPoint);
      this.context.fillText(p[2], ...tangentPoint);
    });
  }

  /**
   * Draws a tooltip after mouseover on an wildRect for secondsToDrawTooltip seconds
   */
  drawTooltip() {
    if (this.framesSinceCursorMoved < this.secondsToDrawTooltip * this.frameRate) {
      // TODO use this time to animate tooltip growing from 0% to 100%
      this.framesSinceCursorMoved++;
    } else {
      const mouseIsOverWildPkmnIndex = this.lastMousePos.indexOfBoundingRect(...this.wildPkmnRects);
      // if mouse is over wild pkmn and not holding a wild pkmn, draw Tooltip
      if (mouseIsOverWildPkmnIndex >= 0 && this.clickedWildPkmnIndex < 0
        && mouseIsOverWildPkmnIndex < this.trainer.wild.pokemon.length) {
        const pokemon = this.trainer.wild.pokemon[mouseIsOverWildPkmnIndex];
        const type0 = `${this.titleCase(pokemon.types[0])}`;
        const type1 = `${this.titleCase(pokemon.types[1])}`;
        const stats = [
          `  HP: ${pokemon.base.HP}`,
          ` ATK: ${pokemon.base.ATK}`,
          ` DEF: ${pokemon.base.DEF}`,
          `SATK: ${pokemon.base.SATK}`,
          `SDEF: ${pokemon.base.SDEF}`,
          ` SPE: ${pokemon.base.SPE}`,
          `Total ${pokemon.getBST()}`
        ];
        this.context.font = `${Font.SIZE_SM * this.scaleRatio()}px ${Font.MONOSPACED}`; // Font size affects textWidth
        const tooltipW = this.context.measureText(stats[6]).width * 3.8;
        const tooltipH = Font.SIZE_SM * this.scaleRatio() * (type1 ? 13.2 : 12.4);
        // Draw tooltip rect
        this.tooltipRect.setAll(...this.lastMousePos, tooltipW, tooltipH);
        this.context.fillStyle = 'rgba(0, 0, 0, 0.7)';
        this.context.fillRect(...this.tooltipRect);
        // Draw pokemon type
        this.context.textAlign = 'center';
        this.context.fillStyle = 'rgba(255, 255, 255, 1)';
        const xPosition = this.lastMousePos.x + this.wildPkmnRects[0].w * 0.5;
        this.context.fillText(type0, xPosition,
          this.lastMousePos.y + this.wildPkmnRects[0].h * 0.2);
        if (type1) {
          this.context.fillText(type1, xPosition,
            this.lastMousePos.y + this.wildPkmnRects[0].h * 0.4);
        }
        // Draw pokemon stats
        stats.forEach((stat, i) => {
          let paddingTop = (type1 ? 0.63 : 0.43) + (i * 0.2);
          if (stat.includes('Total')) { paddingTop += 0.03; }
          this.context.fillText(stat, xPosition,
            this.lastMousePos.y + this.wildPkmnRects[0].h * paddingTop);
        });
        // Draw Stat Chart Rect
        this.chartRect.setAll(
          this.tooltipRect.x + this.tooltipRect.w * 0.37,
          this.tooltipRect.y + this.tooltipRect.h * 0.05,
          this.tooltipRect.w * 0.6,
          this.tooltipRect.w * 0.6);
        this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
        this.context.fillRect(...this.chartRect);
        this.drawStatChart(pokemon);
      }
    }
  }

  /**
   * @param {MouseEvent} event From Event Listener
   */
  onClick(event) {
    const canvasRect = this.canvas.getBoundingClientRect();
    const clickPos   = new Point(event.clientX - canvasRect.left, event.clientY - canvasRect.top);
    if (this.refreshWildRect.containsPoint(clickPos)) {
      this.trainer.buyWildRefresh();
      return;
    }
    const indexOfPkmnOnCursor = this.clickedWildPkmnIndex;
    this.clickedWildPkmnIndex = clickPos.indexOfBoundingRect(...this.wildPkmnRects);
    // if an empty wild pkmn rect is clicked, do nothing
    if (this.clickedWildPkmnIndex >= this.trainer.wild.pokemon.length) {
      this.clickedWildPkmnIndex = -1;
      return;
    }
    // if holding a pokemon and did not click on a wild pkmn again, catch the pokemon if possible
    if (indexOfPkmnOnCursor >= 0 && this.clickedWildPkmnIndex < 0) {
      this.trainer.catchPokemonAtIndex(indexOfPkmnOnCursor);
      // Refresh the sprites in the wild zone
      this.trainer.wild.refreshWildPkmnImgs();
    } else if (indexOfPkmnOnCursor >= 0) {
      this.clickedWildPkmnIndex = -1;
    }
    console.log(`pickUpWildPkmn[${this.clickedWildPkmnIndex}] clickPos(${clickPos.x}, ${clickPos.y})`);
  }

  update() {
    this.context.clearRect(...this.rect);
    this.rect.setAll(
      this.zoneRect.x * this.canvas.width,
      this.zoneRect.y * this.canvas.height,
      this.zoneRect.w * this.canvas.width,
      this.zoneRect.h * this.canvas.height);
    this.context.fillStyle = 'rgba(150, 0, 100, 0.3)';
    this.context.fillRect(...this.rect);

    this.drawWildPokemon();
    // this.drawClickedWildPkmn(); // this is now called in MainPhase.js
    this.drawRefreshWildButton();
    this.drawTooltip();
    this.updateFPS(); // inherited from super class

    this.context.font = `30px ${Font.STYLE}`;
    this.context.fillStyle = 'rgba(0, 0, 0, 0.5)';
    this.context.textAlign = 'center';
    this.context.fillText('Wild', this.rect.x + this.rect.w / 2, this.rect.y + this.rect.h / 2);
  }

}
