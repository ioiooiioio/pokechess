import Point from './Point.js';

export default class Rect {

  /**
   * Bounding box for canvas components and elements
   * @param {number} x left
   * @param {number} y top
   * @param {number} w width
   * @param {number} h height
   */
  constructor(x = 0, y = 0, w = 0, h = 0) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  /**
   * @param {Point} point The point to check
   * @returns {boolean} Returns true if the point is within this Rect
   */
  containsPoint(point) {
    //TODO: Implement - this is currently still the Rect implementation
    return point.x >= this.x
      && point.x <= this.x + this.w
      && point.y >= this.y
      && point.y <= this.y + this.h;
  }

  /**
   * @param {number} x left
   * @param {number} y top
   * @param {number} w width
   * @param {number} h height
   */
  setAll(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  
  draw(ctx, fill) {
    var {x,y,w,h} = this;
    ctx.beginPath();
    ctx.moveTo(x, y - h/2);
    ctx.lineTo(x + w/2, y - h/4);
    ctx.lineTo(x + w/2, y + h/4);
    ctx.lineTo(x, y + h/2);
    ctx.lineTo(x - w/2, y + h/4);
    ctx.lineTo(x - w/2, y - h/4);
    ctx.lineTo(x, y - h/2);
    ctx.stroke();
    if (fill) {
      ctx.fill();
    }
  }

  /**
   * Makes instances of Rect iterable. Enables use of the spread operator, for-of loops, etc.
   * @example
   * const rect = new Rect(2, 4, 5, 7);
   * logRect = (a, b, c, d) => console.log(`${a} ${b} ${c} ${d}`);
   * logPoint(rect.x, rect.y, rect.w, rect.h); // 2 4 5 7
   * logRect(...rect); // 2 4 5 7
   */
  *[Symbol.iterator]() {
    yield this.x;
    yield this.y;
    yield this.w;
    yield this.h;
  }

}
