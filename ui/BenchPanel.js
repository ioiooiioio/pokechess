import ICanDraw from './ICanDraw.js';
import Trainer from '../scripts/class/trainer/Trainer.js';

export default class BenchPanel extends ICanDraw {

  constructor(dim) {
    super(dim);
  }

  /**
   * @param {CanvasRenderingContext2D} ctx 
   * @param {GameState} gameState 
   */
  draw(ctx, gameState) {
    for (let i = 0; i < gameState.bench.length; i++) {
      const pokemon = gameState.bench[i];
      if (pokemon) {
        const img = this.getPokemonImg(gameState, pokemon.id);
        ctx.drawImage(img, this.dim.left + 50 + 75 * i, this.dim.top + 25, 50, 50);
      }
    }
  }

  mouseClick(evt, isRight, gameState) {
    const x = evt.clientX;
    const y = evt.clientY + document.scrollingElement.scrollTop;
    console.log(`mouseClick in BenchPanel at x=${x} y=${y}`);
    if (y < this.dim.top) {
      return;
    }
    const benchIndex = Math.floor((x - 50) / 75);
    if (gameState.bench[benchIndex]) {
      gameState.heldPokemon = gameState.bench[benchIndex].cloneForTrainer(new Trainer(isRight ? 'right' : 'left'));
    }
    console.log(gameState.heldPokemon);
  }

}
