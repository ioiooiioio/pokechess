
import BattleGrid from './ui/BattleGrid.js';
import BenchPanel from './ui/BenchPanel.js';
import { Flamethrower } from './scripts/lib/Skill.lib.js';
import GameSim from './game/GameSim.js';
import Pokemon from './scripts/class/pokemon/Pokemon.js';

// TODO: Import from property file
var serverIp = 'www.esperlab.com';

/**
 * @type {HTMLCanvasElement}
 */
var canvas = document.getElementById('mainCanvas');
/**
 * @type {CanvasRenderingContext2D}
 */
var ctx = canvas.getContext('2d');

var gameSim = new GameSim();

var battleGrid = new BattleGrid({top:0,left:0,width:500,height:500});
var benchPanel = new BenchPanel({top:500,left:0,width:500,height:500});

var gameState = {
  /**
   * @type {Pokemon[][]}
   */
  field: [],
  bench: [],
  caches: {
    img: {
      pokemon: {}
    },
    /**
     * @type {Pokemon[]}
     */
    pokeData: []
  }
};

var func = () => {
  var Braixin = new Pokemon(654).init(Flamethrower);
  var Turtonator = new Pokemon(776).init(Flamethrower);
  var Darkrai = new Pokemon(491).init();
  var Tyrunt = new Pokemon(696).init();
  var Area51 = new Pokemon(605).init();
  gameState.caches.pokeData[654] = Braixin;
  gameState.caches.pokeData[776] = Turtonator;
  gameState.caches.pokeData[491] = Darkrai;
  gameState.caches.pokeData[696] = Tyrunt;
  gameState.caches.pokeData[605] = Area51;
  gameState.bench = [Braixin, Turtonator, Darkrai, Tyrunt, Area51];
  console.log(gameState.bench);
};
func();

function initField() {
  for (var i=0;i<7;i++) {
    gameState.field[i]=[0,0,0,0,0,0,0];
  }
}

initField();

function step() {
  gameSim.step(gameState);
}

function drawHexGrid() {
  battleGrid.draw(ctx, gameState);
  benchPanel.draw(ctx, gameState);
  step();
  setTimeout(drawHexGrid, 500);
}

function setUpWebSocket() {
  window.WebSocket = window.WebSocket || window.MozWebSocket;
  if (!window.WebSocket) {
    console.log("No WebSocket available");
    return;
  }
  var connection = new WebSocket('ws://' + serverIp + ':29073');
  console.log(connection);
  connection.onopen = function () {
    console.log('Connection Opened');
    connection.send('\"Test\"');
  };
  connection.onmessage = function (message) {
    try {
      var jsonMsg = JSON.parse(message.data || message.utf8Data);
      console.log('Message From Server');
      console.log(jsonMsg);
      if (commandProcessor[jsonMsg.label]) {
        commandProcessor[jsonMsg.label](jsonMsg.data, gameState);
      }
    } catch (e) {
      console.log(e);
      console.log('This doesn\'t look like a valid JSON: ', message.data);
      return;
    }
  };
  gameState.serverConnection = connection;
}

var commandProcessor = {
  UPDATE_GAME_STATE: (data, gameState) => {
    gameSim.deserialize(gameState, data);
  }
}

/**
 * @param {MouseEvent} evt 
 * @param {boolean} isRight
 */
function mouseClick(evt, isRight) {
  battleGrid.mouseClick(evt, isRight, gameState);
  benchPanel.mouseClick(evt, isRight, gameState);
}

/**
 * @param {MouseEvent} evt 
 */
function contextMenu(evt) {
  mouseClick(evt, 1);
}

const stepButton = document.getElementById('stepButton');
stepButton.onclick = step;

if (canvas.addEventListener) {
  canvas.addEventListener('onclick', mouseClick, false);
  canvas.addEventListener('click', mouseClick, false);
  canvas.addEventListener('contextmenu', contextMenu, false);
} else {
  canvas.attachEvent('onclick', mouseClick, false);
  canvas.attachEvent('click', mouseClick, false);
  canvas.attachEvent('contextmenu', contextMenu, false);
}

setUpWebSocket();
initField();

setTimeout(drawHexGrid, 1000);
