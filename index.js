
import MainPhase from './scripts/components/MainPhase.js';
import Pokemon from './scripts/class/pokemon/Pokemon.js';

(async () => {
  const mainPhase = new MainPhase();
  mainPhase.update();
})();

/**
 * @ignore Use only to download a new Pokemon.lib.json. If you do, all the 3-d sprites will be lost
 */
const downloadAllPokemon = async () => {
  /**
   * @type {Pokemon[]}
   */
  const lib = [];
  for (let i = 1; i <= 807; i++) {
    const myPkmn = await new Pokemon(i).query();
    lib.push(myPkmn);
    console.log(i);
  }
  const blob = new Blob([JSON.stringify(lib)], { type: 'application/json; encoding=utf8' });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.style.display = 'none';
  a.href = url;
  a.download = 'Pokemon.lib.json';
  document.body.appendChild(a);
  a.click();
  window.URL.revokeObjectURL(url);
}
