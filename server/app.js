
import GameSim from '../game/GameSim.js';
import Pokemon from '../scripts/class/pokemon/Pokemon.js';
import Trainer from '../scripts/class/trainer/Trainer.js';
import WebSocket from 'websocket';
import http from 'http';

const WebSocketServer = WebSocket.server;

const connectionList = [];

const gameSim = new GameSim();

const server = http.createServer((req, res) => { });
server.listen(29073, () => {
  console.log('Server is listening on port 29073');
});

const wsServer = new WebSocketServer({
  httpServer: server
});

wsServer.on('request', (request) => {
  const connection = request.accept();
  console.log(`${new Date()} Connection Opened`);
  connectionList.push(connection);
  connection.send(JSON.stringify({
    label: 'UPDATE_GAME_STATE',
    data: gameSim.serialize(gameState)
  }));
  connection.on('message', (message) => {
    try {
      const jsonMsg = JSON.parse(message.data || message.utf8Data);
      console.log(`${new Date()} Message Received: ${message.data || message.utf8Data}`);
      if (commandProcessor[jsonMsg.label]) {
        commandProcessor[jsonMsg.label](jsonMsg.data, gameState);
      }
    } catch (e) {
      console.error(e);
      console.log('This doesn\'t look like a valid JSON: ', message.data); // data is not a field of IMessage. did you mean .binaryData?
    }
  });
  connection.on('close', (reasonCode, description) => {
    console.log(`${new Date()} Connection closed: ${connection.remoteAddress}`);
  });
});

const gameState = {
  field: []
};

const initField = () => {
  for (let i = 0; i < 7; i++) {
    gameState.field[i] = [0, 0, 0, 0, 0, 0, 0];
  }
}

const step = () => {
  gameSim.step(gameState);
  setTimeout(step, 500);
}

// Make this a class
const commandProcessor = {
  PUT_POKEMON: (params, gameState) => {
    const { row, col, id, trainer } = params;
    if (gameState.field[row] && !gameState.field[row][col]) {
      let pokemon = new Pokemon(id).init();
      pokemon = pokemon.cloneForTrainer(new Trainer(trainer));
      gameState.field[row][col] = pokemon;
      updateAllClients();
    }
  }
}

const updateAllClients = () => {
  const serializedGameState = gameSim.serialize(gameState);
  connectionList.forEach(con => {
    con.send(JSON.stringify({
      label: 'UPDATE_GAME_STATE',
      data: serializedGameState
    }));
  });
}

initField();
step();
